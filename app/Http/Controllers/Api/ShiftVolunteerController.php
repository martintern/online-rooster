<?php

namespace App\Http\Controllers\Api;

use App\Models\Shift;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreShiftVolunteerRequest;
use App\Http\Requests\DestroyShiftVolunteerRequest;

class ShiftVolunteerController extends Controller
{
    public function store(StoreShiftVolunteerRequest $request): void
    {
        Shift::findOrFail($request->shift_id)->volunteer()->associate($request->volunteer_id)->save();
    }

    public function destroy(DestroyShiftVolunteerRequest $request, $id): void
    {
        Shift::findOrFail($id)->update(['volunteer_id' => null]);
    }
}
