<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use App\Models\Shift;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreShiftVolunteerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->isVolunteer()) {
            return Auth::id() == $this->volunteer_id && ! Shift::findOrFail($this->shift_id)->isInThePast();
        }

        if (Auth::user()->isReceptionist()) {
            return Shift::findOrFail($this->shift_id)->stops_at > Carbon::now();
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shift_id' => 'required|exists:shifts,id',
            'volunteer_id' => 'required|exists:users,id'
        ];
    }
}
