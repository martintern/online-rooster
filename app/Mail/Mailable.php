<?php

namespace App\Mail;

use Illuminate\Support\Traits\Macroable;
use Illuminate\Mail\Mailable as IlluminateMailable;

class Mailable extends IlluminateMailable
{
    use Macroable;
}
