<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // CHECKS

    /**
     * Check that this User is a Volunteer
     */
    public function isVolunteer()
    {
        return $this->role == 'volunteer';
    }

    /**
     * Check that this User is a Receptionist
     */
    public function isReceptionist()
    {
        return $this->role == 'receptionist';
    }
}
