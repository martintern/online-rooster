<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'volunteer_id',
    ];

    // CHECKS

    /**
     * Check if this Shift already has a volunteer.
     */
    public function isTaken()
    {
        return $this->volunteer()->exists();
    }

    /**
     * Check if this Shift is in the past.
     */
    public function isInThePast()
    {
        return Carbon::now()->greaterThan($this->stops_at);
    }

    // OTHER

    /**
     * Commit Volunteer to this Shift.
     *
     * @param User $user
     */
    public function commit(User $user)
    {
        if (! $user->isVolunteer()) {
            throw new \LogicException('User must be a Volunteer.');
        }

        if ($this->isTaken()) {
            throw new \LogicException('Shift must not already have a Volunteer.');
        }

        $this->update(['volunteer_id' => $user->id]);
    }

    // RELATIONSHIPS

    /**
     * A Shift belongs to a Volunteer.
     */
    public function volunteer()
    {
        return $this->belongsTo(User::class, 'volunteer_id');
    }
}
