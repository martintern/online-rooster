<?php

use Carbon\Carbon;
use App\Models\User;
use App\Models\Shift;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Shift::class, function (Faker $faker) {
    return [
        'starts_at' => Carbon::parse('last hour'),
        'stops_at' => Carbon::parse('next hour')
    ];
});

$factory->state(Shift::class, 'free', function (Faker $faker) {
    return ['volunteer_id' => null];
});

$factory->state(Shift::class, 'taken', function (Faker $faker) {
    return ['volunteer_id' => factory(User::class)->states('volunteer')->create()->id];
});

// TIME

$factory->state(Shift::class, 'past', function (Faker $faker) {
    return ['stops_at' => Carbon::parse('last hour')];
});

$factory->state(Shift::class, 'present', function (Faker $faker) {
    return [
        'starts_at' => Carbon::parse('last hour'),
        'stops_at' => Carbon::parse('next hour')
    ];
});

$factory->state(Shift::class, 'future', function (Faker $faker) {
    return ['starts_at' => Carbon::parse('next hour')];
});

$factory->state(Shift::class, 'this_month', function (Faker $faker) {
    return ['starts_at' => Carbon::parse('this month')];
});

$factory->state(Shift::class, 'last_month', function (Faker $faker) {
    return ['starts_at' => Carbon::parse('last month')];
});

$factory->state(Shift::class, 'next_month', function (Faker $faker) {
    return ['starts_at' => Carbon::parse('next month')];
});
