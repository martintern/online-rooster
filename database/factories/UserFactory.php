<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => Str::random(10),
    ];
});

$factory->state(App\Models\User::class, 'volunteer', function (Faker $faker) {
    return ['role' => 'volunteer'];
});

$factory->state(App\Models\User::class, 'receptionist', function (Faker $faker) {
    return ['role' => 'receptionist'];
});

$factory->state(App\Models\User::class, 'coordinator', function (Faker $faker) {
    return ['role' => 'coordinator'];
});

$factory->state(App\Models\User::class, 'other', function (Faker $faker) {
    return ['role' => 'other'];
});

$factory->state(App\Models\User::class, 'test', function (Faker $faker) {
    return ['role' => 'test'];
});
