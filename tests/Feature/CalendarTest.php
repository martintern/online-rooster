<?php

namespace Tests\Feature;

use Tests\TestCase;

class CalendarTest extends TestCase
{
    /** @test */
    public function users_can_see_calendar()
    {
        $this->markTestIncomplete();

        $this->get('calendar')->assertSuccessful();
    }
}
