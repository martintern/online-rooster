<?php

namespace Tests\Feature;

use Tests\TestCase;

class HttpsTest extends TestCase
{
    /** @test */
    public function http_requests_are_redirected_to_https()
    {
        $this->markTestIncomplete();
    }

    /** @test */
    public function https_requests_are_not_redirected()
    {
        $this->markTestIncomplete();
    }
}
