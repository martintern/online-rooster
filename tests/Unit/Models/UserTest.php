<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{
    // CHECKS

    /** @test */
    public function itWillConfirmThatUserIsVolunteer()
    {
        $user = factory(User::class)->states('volunteer')->create();

        $this->assertTrue($user->isVolunteer());
    }

    /** @test */
    public function itWillDenyThatUserIsVolunteer()
    {
        $user = factory(User::class)->states('other')->create();

        $this->assertFalse($user->isVolunteer());
    }

    /** @test */
    public function itWillConfirmThatUserIsReceptionist()
    {
        $user = factory(User::class)->states('receptionist')->create();

        $this->assertTrue($user->isReceptionist());
    }

    /** @test */
    public function itWillDenyThatUserIsReceptionist()
    {
        $user = factory(User::class)->states('other')->create();

        $this->assertFalse($user->isReceptionist());
    }
}
