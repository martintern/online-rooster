<?php

namespace Tests\Unit;

use LogicException;
use Tests\TestCase;
use App\Models\User;
use App\Models\Shift;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShiftTest extends TestCase
{
    use RefreshDatabase;

    // CHECKS

    /** @test */
    public function it_will_confirm_shift_is_taken(): void
    {
        $shift = factory(Shift::class)->state('taken')->create();

        $this->assertTrue($shift->isTaken());
    }

    /** @test */
    public function it_will_deny_shift_is_taken(): void
    {
        $shift = factory(Shift::class)->state('free')->create();

        $this->assertFalse($shift->isTaken());
    }

    /** @test */
    public function it_will_confirm_shift_is_in_the_past(): void
    {
        $shift = factory(Shift::class)->state('past')->create();

        $this->assertTrue($shift->isInThePast());
    }

    /** @test */
    public function it_will_deny_shift_is_in_the_past(): void
    {
        $shift = factory(Shift::class)->state('present')->create();

        $this->assertFalse($shift->isInThePast());
    }

    // OTHER

    /** @test */
    public function only_volunteers_can_be_committed_to_a_shift(): void
    {
        $shift = factory(Shift::class)->state('free')->create();
        $user = factory(User::class)->state('other')->create();

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('User must be a Volunteer.');

        $shift->commit($user);
    }

    /** @test */
    public function a_volunteer_cannot_be_committed_to_a_shift_that_is_already_taken()
    {
        $shift = factory(Shift::class)->state('taken')->create();
        $user = factory(User::class)->state('volunteer')->create();

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Shift must not already have a Volunteer.');

        $shift->commit($user);
    }

    /** @test */
    public function a_volunteer_can_be_committed_to_a_free_shift()
    {
        $shift = factory(Shift::class)->state('free')->create();
        $user = factory(User::class)->state('volunteer')->create();

        $shift->commit($user);

        $this->assertTrue($user->is($shift->volunteer));
    }
}
