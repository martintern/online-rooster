<?php

namespace Tests\Unit\Exceptions;

use Exception;
use Tests\TestCase;
use App\Mail\Mailable;
use App\Exceptions\Handler;
use App\Mail\ExceptionOccured;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class ExceptionHandlerTest extends TestCase
{
    /**
     * @test
     * @throws \Exception
     */
    public function handler_sends_mail_on_production_error()
    {
        Mail::fake();
        App::shouldReceive('environment')->with('production')->andReturnTrue();

        $this->reportNewException();

        Mail::assertSent(ExceptionOccured::class, function (Mailable $mail) {
            return $mail->build()->hasTo('martintern@yahoo.com');
        });
    }

    /**
     * @test
     * @throws \Exception
     */
    public function handler_sends_no_mail_on_local_error()
    {
        Mail::fake();
        App::shouldReceive('environment')->with('production')->andReturnFalse();

        $this->reportNewException();

        Mail::assertNotSent(ExceptionOccured::class);
    }

    /**
     * Report a new Exception.
     *
     * @throws \Exception
     */
    private function reportNewException()
    {
        (new Handler($this->app))->report(new Exception());
    }
}
