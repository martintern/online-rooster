<?php

namespace Tests\Unit\Http\Requests;

use App\Models\User;
use App\Models\Shift;
use App\Http\Requests\StoreShiftVolunteerRequest;

final class StoreShiftVolunteerRequestTest extends RequestTestCase
{
    /** @inheritdoc */
    public function request()
    {
        return new StoreShiftVolunteerRequest();
    }

    // AUTHORIZATION

    /** @test */
    public function volunteers_may_not_commit_other_volunteers()
    {
        $shift = factory(Shift::class)->states(['free', 'future'])->create();
        $volunteers = factory(User::class, 2)->states('volunteer')->create();

        $this->be($volunteers[0]);
        $this->assertAuthorizationFails(['shift_id' => $shift->id, 'volunteer_id' => $volunteers[1]->id]);
    }

    /** @test */
    public function volunteers_may_not_commit_to_shifts_in_the_past()
    {
        $shift = factory(Shift::class)->states(['free', 'past'])->create();
        $volunteer = factory(User::class)->states('volunteer')->create();

        $this->be($volunteer);
        $this->assertAuthorizationFails(['shift_id' => $shift->id, 'volunteer_id' => $volunteer->id]);
    }

    /** @test */
    public function volunteers_may_not_commit_themselvesTo_shifts_in_the_present()
    {
        $shift = factory(Shift::class)->states(['free', 'present'])->create();
        $volunteer = factory(User::class)->states('volunteer')->create();

        $this->be($volunteer);
        $this->assertAuthorizationPasses(['shift_id' => $shift->id, 'volunteer_id' => $volunteer->id]);
    }

    /** @test */
    public function receptionists_may_not_commit_volunteers_to_shifts_in_the_past()
    {
        $shift = factory(Shift::class)->states(['free', 'past'])->create();
        $volunteer = factory(User::class)->states('volunteer')->create();
        $receptionist = factory(User::class)->states('receptionist')->create();

        $this->be($receptionist);
        $this->assertAuthorizationFails(['shift_id' => $shift->id, 'volunteer_id' => $volunteer->id]);
    }

    /** @test */
    public function other_users_may_commit_volunteers_to_shift_in_the_past()
    {
        $shift = factory(Shift::class)->states(['free', 'past'])->create();
        $volunteer = factory(User::class)->states('volunteer')->create();
        $otherUser = factory(User::class)->states('other')->create();

        $this->be($otherUser);
        $this->assertAuthorizationPasses(['shift_id' => $shift->id, 'volunteer_id' => $volunteer->id]);
    }

    // VALIDATION

    /** @test */
    public function correct_data_passes()
    {
        $shift = factory(Shift::class)->states(['free', 'past'])->create();
        $volunteer = factory(User::class)->states('volunteer')->create();

        $this->assertValidationPasses(['shift_id' => $shift->id, 'volunteer_id' => $volunteer->id]);
    }

    /** @test */
    public function shift_id_is_required()
    {
        $this->assertValidationFails(['shift_id' => null]);
    }

    /** @test */
    public function shift_id_must_exist()
    {
        $this->assertValidationFails(['shift_id' => 123]);
    }

    /** @test */
    public function volunteer_id_is_required()
    {
        $this->assertValidationFails(['volunteer_id' => null]);
    }

    /** @test */
    public function volunteer_id_must_exist()
    {
        $this->assertValidationFails(['volunteer_id' => 123]);
    }
}
