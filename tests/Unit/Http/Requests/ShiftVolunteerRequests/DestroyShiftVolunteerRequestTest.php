<?php

namespace Tests\Unit\Http\Requests;

use App\Http\Requests\DestroyShiftVolunteerRequest;
use App\Models\Shift;
use App\Models\User;

final class DestroyShiftVolunteerRequestTest extends RequestTestCase
{
    /** @inheritdoc */
    public function request()
    {
        return new DestroyShiftVolunteerRequest();
    }

    /** @test */
    public function volunteers_may_not_destroy_shift_volunteers()
    {
        $volunteer = factory(User::class)->states('volunteer')->create();
        $shift = factory(Shift::class)->states(['taken', 'future'])->create(['volunteer_id' => $volunteer->id]);

        $this->be($volunteer);
        $this->assertAuthorizationFails(['shift_id' => $shift->id, 'volunteer_id' => $volunteer->id]);
    }
}
