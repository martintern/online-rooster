<?php

namespace Tests\Unit\Http\Requests;

use Tests\TestCase;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

abstract class RequestTestCase extends TestCase
{
    /**
     * Returns the Request to be tested.
     *
     * @return \Illuminate\Http\Request
     */
    abstract function request();

    // AUTHORIZATION

    /**
     * Assert authorization fails.
     *
     * @param array $data
     */
    protected function assertAuthorizationFails(array $data = [])
    {
        $request = $this->request()->replace($data);
        $message = 'Failed asserting that authorization fails on: ' . json_encode($data);

        $this->assertFalse($request->authorize(), $message);
    }

    /**
     * Assert authorization passes.
     *
     * @param array $data
     */
    protected function assertAuthorizationPasses(array $data = [])
    {
        $request = $this->request()->replace($data);
        $message = 'Failed asserting that authorization passes on: ' . json_encode($data);

        $this->assertTrue($request->authorize(), $message);
    }

    // VALIDATION

    /**
     * Assert validation fails.
     *
     * @param array $data
     */
    protected function assertValidationFails(array $data = [])
    {
        $validator = Validator::make($data, $this->request()->rules());
        $message = 'Failed asserting that rules fail on: ' . json_encode($data);

        $this->assertTrue($validator->fails(), $message);
    }

    /**
     * Assert validation passes.
     *
     * @param array $data
     */
    protected function assertValidationPasses(array $data = [])
    {
        $validator = Validator::make($data, $this->request()->rules());
        $message = 'Failed asserting that rules pass on: ' . json_encode($data);

        try {
            $validator->validate();
        } catch (ValidationException $e) {
            $this->fail('Failed asserting that rules pass: ' . $validator->messages());
        }


        $this->assertTrue($validator->passes(), $message);
    }
}
