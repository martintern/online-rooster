<?php

namespace Tests;

use App\Mail\Mailable;
use Illuminate\Support\Facades\App;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    /**
     * @inheritdoc
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->macros();
    }

    public function assertEnv(string $environment): void
    {
        $this->assertTrue(App::environment($environment), "Failed asserting that ENV is {$environment}.");
    }

    /**
     * SetUp macros.
     */
    private function macros(): void
    {
        Mailable::macro('assertHasTo', function ($address) {
            TestCase::assertTrue(
                $this->hasTo($address),
                'Failed asserting mail TO field contains address: ' . $address);
        });
    }
}
