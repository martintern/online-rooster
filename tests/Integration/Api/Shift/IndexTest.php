<?php

namespace Tests\Integration\Api\Shift;

use App\Models\User;
use App\Models\Shift;
use Tests\Integration\Api\ApiTestCase;

class IndexTest extends ApiTestCase
{
    /** @test */
    public function route_access_is_protected(): void
    {
        $this->assertGuest()->getJson('api/shifts')->assertStatus(401);
    }

    /** @test */
    public function route_uses_our_custom_request(): void
    {
        $this->markTestIncomplete();
    }

    /** @test */
    public function route_does_what_it_should_do(): void
    {
        $this->withoutExceptionHandling();
        factory(Shift::class)->times(2)->create();

        $this->actingAs(factory(User::class)->create())
            ->getJson('api/shifts')
            ->assertSuccessful()
            ->assertJsonCount(2);
    }
}
