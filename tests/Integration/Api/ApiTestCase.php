<?php

namespace Tests\Integration\Api;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Auth\AuthenticationException;

abstract class ApiTestCase extends TestCase
{
    /**
     * ApiTests must assert route access is protected.
     *
     * @return void
     */
    abstract function route_access_is_protected(): void;

    //TODO: only store and update no?
    /**
     * ApiTests must assert the api route uses a custom Request.
     */
    abstract function route_uses_our_custom_request(): void;

    /**
     * ApiTests must assert the api does what it should.
     */
    abstract function route_does_what_it_should_do(): void;

    /**
     * Assert that given route uses given Request.
     *
     * @param string $method
     * @param string $uri
     * @param string $request
     */
    protected function assertRouteUsesOurCustomRequest(string $method, string $uri, string $request)
    {
        $this->withoutExceptionHandling();

        $message = 'Failed asserting that route uses request class: ' . $request;

        try {
            $this->actingAs(factory(User::class)->create())->json($method, $uri);

            $this->assertTrue($this->app->resolved($request), $message);
        } catch (\Exception $e) {
            $this->assertStringContainsString($request, $e->getTraceAsString(), $message);
        }
    }
}
