<?php

namespace Tests\Integration\Api\ShiftVolunteer;

use App\Models\User;
use App\Models\Shift;
use Tests\Integration\Api\ApiTestCase;
use App\Http\Requests\DestroyShiftVolunteerRequest;

final class DestroyTest extends ApiTestCase
{
    /** @test */
    public function route_access_is_protected(): void
    {
        $this->assertGuest()->deleteJson('api/shift-volunteers/1')->assertStatus(401);
    }

    /** @test */
    public function route_uses_our_custom_request(): void
    {
        $this->assertRouteUsesOurCustomRequest('DELETE', 'api/shift-volunteers/1', DestroyShiftVolunteerRequest::class);
    }

    /** @test */
    public function route_does_what_it_should_do(): void
    {
        $this->withoutExceptionHandling();
        $shift = factory(Shift::class)->states('taken')->create();
        $coordinator = factory(User::class)->states('coordinator')->create();
        $this->assertTrue($shift->volunteer()->exists());

        $this->actingAs($coordinator)->delete("api/shift-volunteers/{$shift->id}")->assertSuccessful();

        $this->assertFalse($shift->fresh()->volunteer()->exists());
    }
}
