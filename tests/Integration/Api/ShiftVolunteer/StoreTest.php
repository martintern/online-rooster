<?php

namespace Tests\Integration\Api\ShiftVolunteer;

use App\Models\User;
use App\Models\Shift;
use Tests\Integration\Api\ApiTestCase;
use App\Http\Requests\StoreShiftVolunteerRequest;

final class StoreTest extends ApiTestCase
{
    /** @test */
    public function route_access_is_protected(): void
    {
        $this->assertGuest()->postJson('api/shift-volunteers')->assertStatus(401);
    }

    /** @test */
    public function route_uses_our_custom_request(): void
    {
        $this->assertRouteUsesOurCustomRequest('POST', 'api/shift-volunteers', StoreShiftVolunteerRequest::class);
    }

    /** @test */
    public function route_does_what_it_should_do(): void
    {
        $shift = factory(Shift::class)->states(['free', 'future'])->create();
        $volunteer = factory(User::class)->state('volunteer')->create();
        $this->assertFalse($shift->volunteer()->exists());

        $this->actingAs($volunteer)->post('api/shift-volunteers', [
            'shift_id' => $shift->id,
            //TODO: make sure volunteer can only post their own id
            //TODO: make another route to only submit yourself?
            'volunteer_id' => $volunteer->id
        ])->assertSuccessful();

        $this->assertTrue($volunteer->is($shift->fresh()->volunteer));
    }
}
