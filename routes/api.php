<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ShiftController;
use App\Http\Controllers\Api\ShiftVolunteerController;

Route::resource('shifts', class_basename(ShiftController::class))->only(['index']);
Route::resource('shift-volunteers', class_basename(ShiftVolunteerController::class))->only(['store', 'destroy']);
